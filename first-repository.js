const MongoClient = require('mongodb').MongoClient;

//On crée un client mongodb qui nous permettra de nous connecter
//au serveur mongo puis de récupérer les db/collections
const client = new MongoClient('mongodb://localhost:27017', {
    useNewUrlParser: true
});

async function getCollection(name) {
    //on se connecte au serveur mongodb
    const server = await client.connect();
    //On utilise la connexion pour récupérer la base de données qu'on
    //souhaite et la collection sur laquelle on veut requêter
    return server.db('simplon').collection(name);
}

async function findAll() {
    const dogCol = await getCollection('dog');
    //On peut faire sur la collection quasiment exactement les
    //mêmes méthodes/requêts qu'avec le client mongodb en cli
    return dogCol.find().toArray();
}
/**
 * On utilise le module.exports pour indiquer qu'est ce qui sera
 * accessible depuis l'extérieur du fichier (c'est l'équivalent du
 * export devant les class/fonctions/variables en js "classique")
 */
module.exports = {
    findAll:findAll,
    getCollection:getCollection
}

/*
function getCollection(name) {
    //on se connecte au serveur mongodb
    return client.connect().then(server => {
        //On utilise la connexion pour récupérer la base de données qu'on
        //souhaite et la collection sur laquelle on veut requêter
        return server.db('simplon').collection(name);
    });
}

function findAll() {
    return getCollection('dog').then(dogCol => {
        //On peut faire sur la collection quasiment exactement les
        //mêmes méthodes/requêts qu'avec le client mongodb en cli
        return dogCol.find().toArray();
    });
}
*/
